import nltk

productions = """ 
S -> NP VP
NP -> 'John' | 'Mary' | D N | NP PP
VP -> V NP | VP PP
V -> 'saw'
D -> 'the'
N -> 'park'
PP -> P NP
P -> 'in'
"""
grammar = nltk.parse_cfg(productions)
tokens = 'John saw Mary in the park'.split()
cp = nltk.ChartParser(grammar)
for (c,tree) in enumerate(cp.nbest_parse(tokens)):
    print tree
print "number of parse trees=", c+1
