
import nltk
from nltk.parse import FeatureEarleyChartParser

def parse_sent(cp, sent):
  trees = cp.nbest_parse(sent.split())
  print sent
  if (len(trees) > 0):
    for tree in trees: print tree
  else:
    print "NO PARSES FOUND"

# load a feature-based context-free grammar
g = nltk.parse_fcfg(open('feat.fcfg').read())
cp = FeatureEarleyChartParser(g)

parse_sent(cp, 'the dog likes children')
parse_sent(cp, 'the dogs like children')
parse_sent(cp, 'the dog like children') # should not get any parses
parse_sent(cp, 'the dogs likes children') # should not get any parses
parse_sent(cp, 'the dog liked children')
parse_sent(cp, 'the dogs liked children')

