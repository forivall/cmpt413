
from nltk.corpus import wordnet as wn
from pprint import pprint

hyp = lambda s:s.hypernyms() # useful to print hypernym tree

line = wn.synset('line.n.3')
print line.name + ':', line.definition
pprint(line.tree(hyp))

for pentagon_sense in wn.synsets('pentagon', 'n'):
    print pentagon_sense.name + ':', pentagon_sense.definition
    pprint(pentagon_sense.tree(hyp))

# compute the minimum distance in wordnet between
# the different senses of pentagon and line
min_distance = -1
for pentagon_sense in wn.synsets('pentagon', 'n'):
    for line_sense in wn.synsets('line', 'n'):
        dist = pentagon_sense.shortest_path_distance(line_sense)
        if min_distance < 0 or dist < min_distance:
            min_distance = dist
print "min distance (pentagon, line) =", min_distance

# is dog more similar to cat or line?
dog = wn.synsets('dog', 'n')[0]
cat = wn.synsets('cat', 'n')[0]
print "dog, cat:", dog.path_similarity(cat)
print "dog, line:", dog.path_similarity(line)

