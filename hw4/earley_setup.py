
import nltk

gram = '''
S -> NP VP
VP -> V NP | V NP PP
V -> "saw" | "ate"
NP -> "John" | "Mary" | "Bob" | Det N | Det N PP
Det -> "a" | "an" | "the" | "my"
N -> "dog" | "cat" | "cookie" | "park"
PP -> P NP
P -> "in" | "on" | "by" | "with"
'''
g = nltk.parse_cfg(gram)
start = g.start()

nonterminals = {}
for rule in g.productions():
    nonterminals[rule.lhs()] = True

def isToken(s):
    if nonterminals.has_key(s):
        return False
    else:
        return True

for rule in g.productions(start):
    dottedrule = (rule, 0)
    rhs = dottedrule[0].rhs()
    print "rule:", dottedrule[0]
    print "rhs len:", len(rhs)
    print "dot is at:", rhs[dottedrule[1]]

def incomplete(state):
    (dottedrule, i, j) = state
    rhs = dottedrule[0].rhs()
    if (dottedrule[1] == len(rhs)): return False
    else: return True

for rule in g.productions(start):
    dottedrule = (rule, 2)
    state = (dottedrule, 0, 0) # span of 0, 0
    if (incomplete(state)): print "incomplete"
    else: print "complete"

def nextCategory(state):
    (dottedrule, i, j) = state
    rhs = dottedrule[0].rhs()
    return rhs[dottedrule[1]]

for rule in g.productions():
    dottedrule = (rule, 0)
    state = (dottedrule, 0, 0) # span of 0, 0
    print dottedrule[0]
    cat = nextCategory(state)
    if isToken(cat):
        print "terminal:", cat
    else:
        for nrule in g.productions(cat):
            print "\t", nrule

