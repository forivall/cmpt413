#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q1: rechunk.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

from nltk import chunk
from nltk.corpus import conll2000

parser = r'''
NP: {<DT|POS|PRP\$>?(<NNP>+<CC><NNP>+<NNS>?)|(<RBR>?((<PDT>?<DT>)|<POS|PRP\$>)?((<RB>?<J.*|VBN>)*((<RB>?<\#|\$>?<CD>)|<N.*>)+)+)|<EX|PRP|WDT|DT|RBS|WP>}
'''

cp = chunk.RegexpParser(parser)
print 'test:{0:.6f}'.format(chunk.accuracy(cp, conll2000.chunked_sents('test.txt', chunk_types=('NP',))))

# import difflib
# import re
# ##np_matcher = re.compile(r'\(NP ')
# from itertools import islice, ifilter
# for i in xrange(78,1000):
    # try:
        # raw_input()
    # except KeyboardInterrupt:
        # break
    # gold_tree = conll2000.chunked_sents('train.txt', chunk_types=('NP',))[i]
    # ##print '\n'.join(ifilter(np_matcher.search,str(gold_tree).splitlines()))
    # print 'diff for {0}:'.format(i)
    # print '\n'.join(islice(difflib.unified_diff(str(gold_tree).splitlines(), str(cp.parse(gold_tree.flatten())).splitlines()),2,None))
# print gold_tree
# print cp.parse(gold_tree.flatten())
