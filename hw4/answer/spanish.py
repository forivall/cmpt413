#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q10: spanish.py # file in answer dir: spanish.fcfg
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>, Max Whitney <mwhitney@sfu.ca>, Anoop Sarkar <anoop@sfu.ca>
#

import nltk
from nltk.parse import FeatureEarleyChartParser
import sys

def parse_sent(cp, sent):
  trees = cp.nbest_parse(sent.split())
  if (len(trees) > 0):
    for tree in trees: print tree
  else:
    print "NO PARSES FOUND"

# load a feature-based context-free grammar
try:
    f = open('spanish.fcfg')
except IOError:
    f = open('answer/spanish.fcfg')
g = nltk.parse_fcfg(f.read())
f.close()
cp = FeatureEarleyChartParser(g)

s = sys.stdin.read()
parse_sent(cp, s)
