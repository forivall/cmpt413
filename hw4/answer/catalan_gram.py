#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  q6: catalan_gram.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>, Max Whitney <mwhitney@sfu.ca>, Anoop Sarkar <anoop@sfu.ca>
#
import nltk
# implement the catalan function and insert the right value for the
# parameter n below so that you can predict the total number of parses
# for an input sentence from sys.stdin
# print "catalan number =", catalan(n)
grammar = nltk.parse_cfg("""
S -> NP VP
NP -> NN | NN PP
NN -> 'Kafka' | 'the' N
VP -> V NP | V NP PP
V -> 'ate'
N -> 'box' | 'table' | 'cookie'
PP -> P NP
P -> 'in' | 'on'
""")
# 5
# Kafka ate the cookie in the box on the table
# 14
# Kafka ate the box on the cookie in the box on the table
# 42
# the box ate the cookie in Kafka on the table in the box on the table

# tokens = 'Kafka ate the cookie in the box on the table'.split()
import sys
sentence = sys.stdin.read()
tokens = sentence.split()
# cp = nltk.ChartParser(grammar, nltk.parse.chart.TD_STRATEGY)
cp = nltk.ChartParser(grammar)
for (c,tree) in enumerate(cp.nbest_parse(tokens)):
    print tree
print "number of parse trees=", c+1
