#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q8: earley.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>, Max Whitney <mwhitney@sfu.ca>, Anoop Sarkar <anoop@sfu.ca>
#

import sys
from collections import defaultdict
import functools
from itertools import ifilterfalse
import nltk

sentence_str = sys.stdin.read()
sentence_tok = sentence_str.split()

gram = '''
S -> NP VP
VP -> V NP | V NP PP
V -> "saw" | "ate"
NP -> "John" | "Mary" | "Bob" | Det N | Det N PP
Det -> "a" | "an" | "the" | "my"
N -> "dog" | "cat" | "cookie" | "park"
PP -> P NP
P -> "in" | "on" | "by" | "with"
'''
g = nltk.parse_cfg(gram)
start = g.start()

class DottedRule(object):
    def __init__(self, rule, dot):
        self.rule = rule
        self.dot = dot
    token_before_dot = property(lambda self:self.rule.rhs()[self.dot-1] 
                    if self.dot > 0 else None)
    token_after_dot = property(lambda self:self.rule.rhs()[self.dot] 
                    if self.dot < len(self.rule.rhs()) else None)
    def advance_dot(self):
        return DottedRule(self.rule, self.dot+1)
    def __getitem__(self, index):
        if index == 0:
            return self.rule
        if index == 1:
            return self.dot
        raise IndexError
    def __iter__(self):
        return iter((self.rule, self.dot))
    def __hash__(self):
        return hash((self.rule, self.dot))
    def __eq__(self, other):
        return self.rule == other.rule and self.dot == other.dot
    def __ne__(self, other):
        return not self.__eq__(other)
    def __str__(self):
        s = '%s ->' % (self.rule.lhs(),) 
        for i, elt in enumerate(self.rule.rhs()): 
            if i == self.dot:
                s += ' *'
            s += ' %s' % (elt,) 
        if self.dot >= len(self.rule.rhs()):
            s += ' *'
        return s 

##def autoexpand_state_arg(f):
##    @functools.wraps(f)
##    def wrapper(state, *args, **kwargs):
##        (dottedrule, i, j) = state
##        return f(dottedrule, i, j, *args, **kwargs)
##    return wrapper

def print_me(f):
    @functools.wraps(f)
    def wrapper(state, *args, **kwargs):
        print '{0}: {1}'.format(f.__name__, state_str(state))
        return f(state, *args, **kwargs)
    return wrapper

# Utility Functions
def state_str(state):
    return '{0} [{1}, {2}]'.format(*state)

def incomplete(state):
    (dottedrule, i, j) = state
    rhs = dottedrule[0].rhs()
    return not (dottedrule[1] == len(rhs))

def nextCategory(state):
    (dottedrule, i, j) = state
    rhs = dottedrule[0].rhs()
    return rhs[dottedrule[1]]

def final(state):
    (dottedrule, i, j) = state
    return (dottedrule.rule.lhs() == start and
            not incomplete(state) and
            j == len(sentence_tok))

def get_isToken(grammar):
    nonterminals = {}
    for rule in g.productions():
        nonterminals[rule.lhs()] = True
    return lambda s: not nonterminals.has_key(s)
isToken = get_isToken(g)
# program state functions

chart = defaultdict(list)
    
def enqueue(state, j):
    if state not in chart[j]:
        print 'new chart[{0}]: {1}'.format(j, state_str(state))
        chart[j].append(state)

marks = {}

def mark(state):
    marks[state] = True

def is_marked(state):
    return marks.get(state, False)
        
### core functions

@print_me
def predictor(state):
    (dottedrule, i, j) = state
    for rule in g.productions(lhs=dottedrule.token_after_dot):
        enqueue((DottedRule(rule, 0), j, j), j)
@print_me
def scanner(state, tokens):
    (dottedrule, i, j) = state
    a = dottedrule.token_after_dot
    if tokens[j] == a:
        enqueue((dottedrule.advance_dot(), i, j+1), j+1)
@print_me
def completer(state):
    (dottedrule, j, k) = state
    assert(dottedrule.token_after_dot is None)
    for chartstate in chart[j]:
        (chart_rule, chart_i, chart_j) = chartstate
        if chart_rule.token_after_dot == dottedrule.rule.lhs():
            enqueue((chart_rule.advance_dot(), chart_i, k), k)
            
def earley(tokens, grammar):
    N_plus1 = len(tokens)
    for rule in g.productions(start):
        enqueue((DottedRule(rule, 0), 0, 0), 0)
    for j in range(N_plus1 + 1):
        test_i = 0
        for state in ifilterfalse(is_marked, chart[j]):
            test_i += 1
            (dottedrule, i, j) = state
            mark(state)
            next_token = dottedrule.token_after_dot
            # if isinstance(next_token, nltk.Nonterminal):
            if not isToken(next_token) and next_token is not None:
                predictor(state)
            elif isToken(next_token) and next_token is not None and j < N_plus1:
                scanner(state, tokens)
            elif next_token is None:
                completer(state)
        # print test_i, len(chart[j])
    return bool(any(state[0].rule.lhs() == start for state in chart[N_plus1]))
    
if __name__ == '__main__':
    print ('no', 'yes')[earley(sentence_tok, g)]
####

# list productions 
##for rule in g.productions(start):
##    dottedrule = (rule, 0)
##    rhs = dottedrule[0].rhs()
##    print "rule:", dottedrule[0]
##    print "rhs len:", len(rhs)
##    print "dot is at:", rhs[dottedrule[1]]
##    
##for rule in g.productions(start):
##    dottedrule = (rule, 2)
##    state = (dottedrule, 0, 0) # span of 0, 0
##    print ("in" if incomplete(state) else "") + "complete"
##
##for rule in g.productions():
##    dottedrule = (rule, 0)
##    state = (dottedrule, 0, 0) # span of 0, 0
##    print dottedrule[0]
##    cat = nextCategory(state)
##    if isToken(cat):
##        print "terminal:", cat
##    else:
##        for nrule in g.productions(cat):
##            print "\t", nrule

