#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q12: wordnet_ancestor_all.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>, Max Whitney <mwhitney@sfu.ca>, Anoop Sarkar <anoop@sfu.ca>
#
# lowest_common_hypernyms modified from
# Copyright (C) 2001-2012 NLTK Project
# Author: Steven Bethard <Steven.Bethard@colorado.edu>
#         Steven Bird <sb@csse.unimelb.edu.au>
#         Edward Loper <edloper@gradient.cis.upenn.edu>
#         Nitin Madnani <nmadnani@ets.org>
# URL: <http://www.nltk.org/>
# For license information, see LICENSE.TXT (APACHE LICENSE)

import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('word1')
    parser.add_argument('word2')
    parser.add_argument('-v', action='store_true')
    args = parser.parse_args()
else:
    args = argparse.Namespace(word1='pentagon', word2='line', v=True)

def vprint(*arg):
    if args.v:
        for x in arg:
            print x,
        print
        
####

try:
    from nltk.corpus import wordnet as wn
except KeyboardInterrupt:
    print 'exited while importing'
    import sys
    sys.exit()
from pprint import pprint

hyp = lambda s:s.hypernyms() # useful to print hypernym tree
##
##line = wn.synset('line.n.3')
##print line.name + ':', line.definition
##pprint(line.tree(hyp))
##
# for word1_sense in wn.synsets(args.word1, 'n'):
    # print word1_sense.name + ':', word1_sense.definition
    # pprint(word1_sense.tree(hyp))

# compute the minimum distance in wordnet between
# the different senses of pentagon and line
def mainfunc(args):
    min_distance = -1
    min_senses = []
    for word1_sense in wn.synsets(args.word1, 'n'):
        for word2_sense in wn.synsets(args.word2, 'n'):
            dist = word1_sense.shortest_path_distance(word2_sense)
            if min_distance < 0 or dist < min_distance:
                min_distance = dist
                min_senses[:] = [(word1_sense, word2_sense)]
            elif dist == min_distance:
                min_senses.append((word1_sense, word2_sense))
    
    print ([lowest_common_hypernyms(s1,s2) for s1, s2 in min_senses], min_distance)
    if args.v:
        if True:
            s1, s2 = min_senses[2]
#        for s1, s2 in min_senses:
            print s1, s2
            lch = lowest_common_hypernyms(s1,s2)
            print lch
            pprint(s1.hypernym_paths(), width=500)
            pprint(s2.hypernym_paths(), width=500)
            for lch_ in lch:
                print lch_.hypernym_paths()
#            pprint(s1.tree(hyp))
#            pprint(s2.tree(hyp))

def lowest_common_hypernyms(self, other):
    """Get the lowest synset that both synsets have as a hypernym."""
    self_hypernyms = self._iter_hypernym_lists()
    other_hypernyms = other._iter_hypernym_lists()

    synsets = set(s for synsets in self_hypernyms for s in synsets)
    others = set(s for synsets in other_hypernyms for s in synsets)
    synsets.intersection_update(others)
#    print [(s, min_depth(s), self.shortest_path_distance(s), 
#            other.shortest_path_distance(s)) for s in synsets]
    try:
        max_depth = max(s.max_depth() for s in synsets)
        return [s for s in synsets if s.max_depth() == max_depth]
#        min_depth = min(distance(self, s) for s in synsets)
#        return [s for s in synsets if distance(self, s) == min_depth]
    except ValueError:
        return []

#def distance(self, other):
#    return min(d for s, d in self.hypernym_distances() if s == other)

if __name__ == '__main__':
    try:
        mainfunc(args)
    except KeyboardInterrupt:
        print 'exited while running'










