#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  q2: tree_constituents.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

from nltk.tree import Tree
sent = '(S (S (NP Kim) (V arrived)) (conj or) (S (NP Dana) (V left)))'
tree = Tree.parse(sent)

def walk_tree(tree):
    print tree.pprint(margin=40, indent=1, nodesep=':', quotes=True)
    for child in tree:
        if isinstance(child, Tree):
            walk_tree(child)

walk_tree(tree)
