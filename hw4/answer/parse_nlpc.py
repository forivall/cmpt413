#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  q5: parse_nlpc.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

import nltk

productions = """
N -> 'natural' | 'language' | 'processing' | 'course' | N N
"""
grammar = nltk.parse_cfg(productions)
tokens = 'natural language processing course'.split()
cp = nltk.ChartParser(grammar)
for tree in cp.nbest_parse(tokens):
    print tree
