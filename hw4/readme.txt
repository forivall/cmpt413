
The filenames for each question are given below.  We will grade
only the questions marked with a dagger in the PDF file.  They are
marked with + below. 

Test your programs by running: "python check-hw4.py". Run without
arguments and it will print out a detailed help text about usage
and grading.

When you submit HW4 on courses.cs.sfu.ca submit the following URL:

https://punch.cs.sfu.ca/svn/CMPT413-1121-(your-userid)/hw4/answer

%%

+q1: rechunk.py
q2: tree_constituents.py
q5: parse_nlpc.py
q6: catalan_gram.py
+q8: earley.py
+q10: spanish.py # file in answer dir: spanish.fcfg
+q12: wordnet_ancestor_all.py

