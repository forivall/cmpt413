import nltk

# NP -> 'John' | 'Mary' | 'Bob' | Det N | NP PP
productions = """ 
S -> NP VP
NP -> 'John' | 'Mary' | 'Bob' | Det N | Det N PP
VP -> V NP | V NP PP
V -> 'saw' | 'ate'
Det -> 'a' | 'an' | 'the' | 'my'
N -> 'dog' | 'cat' | 'cookie' | 'park'
PP -> P NP
P -> 'in' | 'on' | 'by' | 'with'
"""
grammar = nltk.parse_cfg(productions)
rd_parser = nltk.RecursiveDescentParser(grammar)
sent = 'Mary saw Bob'.split()
for p in rd_parser.nbest_parse(sent):
    print p
