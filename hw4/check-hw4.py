import check
import os
import difflib
import re

def edgews_normalize(*parts):
    def filter(x):
        x = [l.strip() for l in x]
        return [l + '\n' for l in x if l != '']
    return [filter(x) for x in parts]

def diff_almost_exact(a, b, output):
    a, b = edgews_normalize(a, b)
    if a != b:
            output.write("Diff in output:\n")
            output.writelines(difflib.unified_diff(a, b))
            return False
    return True

def diff_almost_exact_uniq(a, b, output):
    return diff_almost_exact(set(a), set(b), output)

def diff_unordered(a, b, output):
    a, b = edgews_normalize(a, b)
    a, b = sorted(a), sorted(b)
    if a != b:
            output.write("Diff in sorted output:\n")
            output.writelines(difflib.unified_diff(a, b))
            return False
    return True

def diff_report_scores(a, b, output):
    a, b = edgews_normalize(a, b)
    for (i,j) in zip(a, b):
        x = i.rstrip().split(':')
        y = j.rstrip().split(':')
        (k1, v1) = (" ".join(x[:-1]), x[-1])
        (k2, v2) = (" ".join(y[:-1]), y[-1])
        print "%s: reference:%s yours:%s difference: %lf" % (k1, v1, v2, float(v2)-float(v1))
    return True

checks = {
    "rechunk": (diff_report_scores, None, []),
    "tree_constituents": (diff_almost_exact, None, []),
    "parse_nlpc": (diff_unordered, None, []),
    ## "catalan_gram": (diff_almost_exact, None, []),
    "earley": (diff_unordered, None, []),
    "spanish": (diff_almost_exact, None, []),
    "wordnet_ancestor_all": (diff_almost_exact, None, []),
}

check.check_all(checks)
