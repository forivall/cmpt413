#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  final project weighted grammar generator from treebanks
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#
import functools

from django.conf import settings
try:
    settings.configure(DEBUG=True, TEMPLATE_DEBUG=True)
except RuntimeError:
    pass
from django.template.base import Node, TemplateSyntaxError
from django.template import defaulttags

register = defaulttags.register

class PretendNode(Node):
    def __init__(self, subterminal_name, nodelist):
        self.subterm_name = subterminal_name
        self.nodelist = nodelist
    def render(self, context):
        output = self.nodelist.render(context)
        outlines = []
        for line in output.splitlines():
            if not line.strip() or line.startswith('#'):
                outlines.append(line)
                continue
            f = line.split(None, 2)
            count, tag, terminals = f
            outlines.append('{:<4} {:<7} {}'.format(count, self.subterm_name, terminals))
        return '\n'.join(outlines)
@register.tag(name="pretend")
def do_pretend(parser, token):
    try:
        tag_name, subterminal_name = token.split_contents()
        nodelist = parser.parse(('endpretend',))
        parser.delete_first_token()
    except ValueError:
        raise TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
    return PretendNode(subterminal_name, nodelist)

# http://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
class memoized(object):
    '''Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned 
    (not reevaluated).
    '''
    def __init__(self, func):
       self.func = func
       self.cache = {}
    def __call__(self, *args):
       try:
          return self.cache[args]
       except KeyError:
          value = self.func(*args)
          self.cache[args] = value
          return value
       except TypeError:
          # uncachable -- for instance, passing a list as an argument.
          # Better to not cache than to blow up entirely.
          return self.func(*args)
    def __repr__(self):
       '''Return the function's docstring.'''
       return self.func.__doc__
    def __get__(self, obj, objtype):
       '''Support instance methods.'''
       return functools.partial(self.__call__, obj)
    def tweak(self, value, *args):
        self.cache[args] = value
