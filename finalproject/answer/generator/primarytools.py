#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  final project weighted grammar generator from treebanks
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

# time permitting, I would have tools for manually writing grammar rules here, like
# chomsky-normal form automation, finding holes where a rule could go, etc.
