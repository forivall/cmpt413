#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  final project weighted grammar generator from treebanks
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

import os.path

#MYPATH = os.path.dirname(__file__)
MYPATH = os.path.normpath((os.path.join(os.path.dirname(__file__), '..')))

# part 1
# get most likely tag from brown corpus for each word in vocab.

S1_OUT_FILENAME = os.path.join(MYPATH, 'S1-autogen.gr')
S2_OUT_FILENAME = os.path.join(MYPATH, 'S2-autogen.gr')
VOCAB_OUT_FILENAME = os.path.join(MYPATH, 'Vocab-autogen.gr')
VOCAB_IN_FILENAME = os.path.join(MYPATH, '../Vocab.gr')
VOCAB_FILENAME = os.path.join(MYPATH, 'Vocab.gr')

TREEBANK_FILENAME_L = os.path.join(MYPATH, '../treebank_wsj_treep_nonull.txt')
TREEBANK_FILENAME_S = os.path.join(MYPATH, '../atis3.treebank')
