#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  final project weighted grammar generator from treebanks
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#
from collections import defaultdict
from itertools import ifilter
import functools

import nltk

from const import *
from util import memoized
from backoffgen import collect_subterminals

#### part 3: generate probabilistic grammar based on an input treebank
def collect_trees(treefile_n = TREEBANK_FILENAME_S):
    tf = open(treefile_n)
    trees = []
    for line in (l for l in tf if not l.startswith('#')):
        trees.append(nltk.tree.Tree.parse(line))
    return trees
collect_trees_L = lambda:collect_trees(TREEBANK_FILENAME_L)

node_to_subterm_map = {
    '.':'EoS',
    ',':'EoB',
    ':':'EoC',
    '--':'EoF',
    'WP':'WPS',
    'JJS':'JJT',
    'PDT':'ABN',
    'WP$':'WP',
    'PRP$':'PP$',
    'IN':'CS',
    'NNPS':'NPS',
    'NNP':'NNP', # since it's kinda a special case
    # there are no RBT's in our vocab
    #'RBS':'RBT',
}
subterm_to_node_map = {v:k for k,v in node_to_subterm_map.iteritems()}

ignored_nodes = set(('RBS','RBR','SYM','LS', 'FW',
# quote things are for quotations, like he said ``blah''
        "``-A","''-A", '``', "''",
        # hash is the british pound
        # rrb, lrb are round brackets
        '#', '$', '-RRB-', '-LRB-',))

def has_target(target, tree):
    if isinstance(tree, nltk.tree.Tree):
        if target == tree.node:
            return True
        else:
            return any(has_target(target, subtree) for subtree in tree)
    else:
        return False
def first_target(target, tree):
    if isinstance(tree, nltk.tree.Tree):
        if target == tree.node:
            return tree
        else:
            for tree in (first_target(target, subtree) for subtree in tree):
                if tree is not None:
                    return tree
    return None
def trees_with(target, treebank):
    return (tree for tree in treebank if has_target(target, tree))
def _subtrees_with(target, treebank):
    return ifilter(None, (first_target(target, tree) for tree in treebank))

def first(iterable):
    try:
        return iterable.next()
    except StopIteration:
        return None
first_tree_with = lambda target, treebank: first(trees_with(target, treebank))
first_subtree_with = lambda target, treebank: first(_subtrees_with(target, treebank))
        
def get_subtrees_for_norecurse(target, tree):
    subtrees = []
    def walk_tree(tree):
        if isinstance(tree, nltk.tree.Tree):
            if target in tree.node:
                subtrees.append(tree)
            else:
                for subtree in tree:
                    walk_tree(subtree)
    walk_tree(tree)
    return subtrees
def get_subtrees_for(terminal, tree):
    return list(tree.subtrees(lambda t:t.node.startswith(terminal)))
def to_rules_simple(tree, all_subtree=False):
    target = tree.node
    tree.chomsky_normal_form(childChar=u'+')
    rules = []
    #subtrees = [tree] + get_subtrees_for(target, tree)
    subtrees = []
    def walk_tree(tree):
        if isinstance(tree, nltk.tree.Tree):
            if tree.node.startswith(target):
                subtrees.append(tree)
            for subtree in tree:
                walk_tree(subtree)
    walk_tree(tree)
    
    for st in subtrees:
        rules.append('{:<4} {:<7} {} {}'.format(1, st.node, st[0].node, st[1].node if len(st) > 1 else '').strip())
    return sorted(set(rules))
    #return rules  

def _to_vocab_subterm(symbol, subterminals):
    if symbol in node_to_subterm_map:
        return node_to_subterm_map[symbol] + '_t'
    if (symbol + '_t') in subterminals:
        return symbol + '_t'
    return symbol
to_vocab_subterm = functools.partial(_to_vocab_subterm, subterminals=set(collect_subterminals()))
def to_rule(production, n=1):
    return '{:<4} {:<7} {}'.format(n, production.lhs(), ' '.join(to_vocab_subterm(nt.symbol()) for nt in production.rhs()))
def to_rules(treebank, chomsky=True):
    # make sure to_vocab_subterm is up to date 
    #  -- kinda ugly, but it works and i don't wanna refactor
    global to_vocab_subterm
    to_vocab_subterm = functools.partial(_to_vocab_subterm, subterminals=set(collect_subterminals()))
    
    if chomsky:
        for tree in treebank:
            tree.chomsky_normal_form(childChar=u'+')
    productions = [p
            for tree in treebank
            for p in tree.productions() if p.is_nonlexical()]
    return [to_rule(p, 
            #sum(len(st.flatten()) for st in subtrees_matching(p, treebank))
            productions.count(p))
            for p in sorted(filter_productions(productions))]
def filter_productions(productions):
    fixed_production_set = set(productions)
    @memoized
    def sum_count(lhs):
        return sum(productions.count(p) for p in fixed_production_set if p.lhs().symbol() == lhs)
    
    production_set = set(productions)
    local_ignored = set(ignored_nodes)
    # 1. filter out ignored nodes
    while(len(local_ignored) > 0):
        print '.',
        removed_productions = set()
        for p in production_set:
            if any(nt.symbol() in local_ignored for nt in p.rhs()):
                removed_productions.add(p)
        filter_lhs = defaultdict(int)
        for r in removed_productions:
            filter_lhs[r.lhs().symbol()] += productions.count(r)
        for lhs, v in filter_lhs.items()[:]:
            total_productions = sum_count(lhs)
            if v < total_productions:
                sum_count.tweak(total_productions - filter_lhs[lhs], lhs)
                del filter_lhs[lhs]
        production_set -= removed_productions
        local_ignored = set(filter_lhs.keys())
    return production_set

def subtrees_matching(production, treebank):
    return (subtree
            for tree in treebank
            for subtree in tree.subtrees(lambda tree: (
                    tree.node == production.lhs().symbol() and
                    [st.node for st in tree] == [nt.symbol() for nt in production.rhs()])))
def get_count(p, treebank):
    return sum(len(st.flatten()) for st in _subtrees_with(p, treebank))
def get_rules_for(target, treebank):
    rules = [r
            for tree in treebank
            for st in get_subtrees_for_norecurse(target, tree)
            for r in to_rules(st)]
    return sorted(set(rules))

# todo: write a function that gets all subterminals defined in a treebank
def get_subterminals(treebank):
    subterminals = set()
    for tree in treebank:
        for p in tree.productions():
            if p.is_lexical():
                subterminals.add(p.lhs().symbol())
    return subterminals

def generate_S1(rules=None):
    if rules is None:
        treebank = collect_trees()
        rules = to_rules(treebank)
    with open(S1_OUT_FILENAME, 'w') as f:
        f.write('# THIS FILE HAS BEEN AUTOGENERATED; ANY CHANGES MAY BE LOST\n')
        for rule in rules:
            f.write(rule)
            f.write('\n')

if __name__ == '__main__':
    generate_S1(to_rules(collect_trees_L()))
