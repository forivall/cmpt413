#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  final project weighted grammar generator from treebanks
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

from collections import defaultdict
import re

import nltk

from const import VOCAB_OUT_FILENAME, VOCAB_IN_FILENAME
from data import tag_cfd, brown_cfd_notsimp


tags_for = lambda word: [(tag, tag_cfd[word][tag]) for tag in tag_cfd[word]]
tags_for_brown = lambda word: [(tag, brown_cfd_notsimp[word][tag]) for tag in 
        brown_cfd_notsimp[word]]

construct_vocab_brown = lambda: construct_vocab(brown_cfd_notsimp)
def construct_vocab(local_tag_cfd=None):
    if local_tag_cfd == None:
        local_tag_cfd = tag_cfd
    multis = set()
    
    # TODO: put probabilities into the
    
    # TODO: finish this conversion table between brown tags and non-terminals
    tag_to_nonterminal = {
            '.':'Punc',
            '*':'Not',
            ':':'Coln',}
    
    newv = open(VOCAB_OUT_FILENAME, 'w')
    word_count = defaultdict(int)
    omit_count = defaultdict(int)
    oldv = open(VOCAB_IN_FILENAME)
    for line in oldv:
        if line.startswith('#') or len(line.strip()) <= 0:
            newv.write(line)
            continue
        f = line.split(None, 2)
        count, tag, terminals = f
        if len(terminals.split()) > 1:
            multis.update(terminals.split())
            newv.write(line)
            continue
        word = terminals.strip()
        if tag in multis:
            newv.write(line)
            continue
        if not local_tag_cfd[word]:
            word_count[word] += 1
            newv.write(line)
            continue
        try:
            newtag = list(local_tag_cfd[word])[word_count[word]]
        except IndexError:
            omit_count[word] += 1
            continue
        finally:
            word_count[word] += 1
        if newtag in tag_to_nonterminal:
            nonterminal = tag_to_nonterminal[newtag]
        else:
            nonterminal = newtag + '_tag'
            tag_to_nonterminal[newtag] = nonterminal
        newv.write('{:<4} {:<7} {}'.format(count, nonterminal, terminals))
    
    oldv.close()    
    newv.close()
    
    
    print 'Word counts:'
    for word, count in sorted(word_count.iteritems(), key=lambda x:x[0], reverse=True):
        print ("{:<10} {}".format(word+':', count) + 
                (" (omitted {} times)".format(omit_count[word]) if word in omit_count else ""))
        
    print 'wrote {}'.format(VOCAB_OUT_FILENAME)

#if __name__ == '__main__':
    #construct_vocab()
            
# part 1.5
# hand-tune results based on grail.txt contexts

# from nltk book (ch2 or 3 i think)
class IndexedText(object):
    def __init__(self, text):
        self._re = re.compile(r"(y')(.*)")
        self._text = text
        self._index = nltk.Index((self._stem(word), i) for (i, word) in 
                                 enumerate(tok for wrd in text for tok in self._split(wrd)))

    def concordance(self, word, width=40):
        key = self._stem(word)
        wc = width/4                # words of context
        for i in self._index[key]:
            lcontext = ' '.join(self._text[i-wc:i])
            rcontext = ' '.join(self._text[i:i+wc])
            ldisplay = '%*s'  % (width, lcontext[-width:])
            rdisplay = '%-*s' % (width, rcontext[:width])
            print ldisplay, rdisplay
    
    def _split(self, word):
        m = self._re.match(word)
        if m:
            return m.groups()
        return (word,)
    
    def _stem(self, word):
        return word.lower()
    
grail = nltk.corpus.webtext.words('grail.txt')
grail_i = IndexedText(grail)
#contexts_of = lambda word:grail_i.concordance(word)

grailraw = nltk.corpus.webtext.raw('grail.txt')
def contexts_of(word):
    i = 0
    while i < len(grailraw):
        try:
            i = grailraw.index(word, i)
            print grailraw[max(0,i-40):min(len(grailraw)-1,i+40)].replace('\n', ' ')
            i += 1
        except ValueError: 
            break


