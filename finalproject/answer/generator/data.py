#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  final project weighted grammar generator from treebanks
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

# since these operations takes a while, I moved it here, so I can keep a
# IPython session running with this data loaded in memory and just reload the 
# generator module, rather than launching 'generator.py' from the shell every time.
import nltk
import os.path
from const import MYPATH
#brown_cfd = nltk.ConditionalFreqDist((nltk.corpus.brown.tagged_words(simplify_tags=True)))
brown_cfd_notsimp = nltk.ConditionalFreqDist((nltk.corpus.brown.tagged_words(simplify_tags=False)))
#treebank_cfd_notsimp = nltk.ConditionalFreqDist((nltk.corpus.treebank.tagged_words(simplify_tags=False)))
#cfd_notsimp = nltk.ConditionalFreqDist((nltk.corpus.treebank.tagged_words(simplify_tags=True)))
treebank_real = nltk.corpus.BracketParseCorpusReader(
        os.path.join(MYPATH, '..'),['treebank_wsj_treep_nonull.txt'], 
        comment_char='#',detect_blocks='sexpr',
        tag_mapping_function=nltk.tag.simplify_wsj_tag)
 
tag_cfd = nltk.ConditionalFreqDist(treebank_real.tagged_words())
