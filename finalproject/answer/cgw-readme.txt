
CMPT 413 Final Project
====

### Jordan Klassen - jjk5@sfu.ca - 301066175

My grammar development methodology generally follows a hybrid generate 
grammars/hand-tune grammars methodology.

First, I use the given wsj/upenn treebank corpus
(`treebank_wsj_treep_nonull.txt.bz2`) and the brown corpus to tag the Vocab 
according to standard tags. I then manually tune those to a smaller, more 
comprehensible set. I check what the most likely tag(s) are by checking each 
word's context in `nltk.corpus.webtext.words('grail.txt')`, since that's 
probably where the sentences for cgw-holygrail are from.

Second, I generate the backoff grammar based on the preterminals I come up with 
in the first step. That was an easy step.

Third, I generate a primary grammar by mining the provided atis3 treebank 
(mining the entire penn treebank would make my grammar way too big and was 
taking really long). I then tune that to the test data and holy grail examples,
adding a few rules here and there. If I had more time, I would remove some 
unessesary rules.

Lastly, I tweak the weights by intuition and by analysis of the treebank corpus 
(using my freqdist generated in my first step). -- I didn't get to this point, 
since it's probably time-consuming either manually doing it or writing a script 
to help me.

My grammar generally gets about -9 to -13 log entropy. I'm happy with that, 
given how much I procrastinated.

Code
----

All of my generation code is in the `generator` module. There's a few comments that used to outline the code; `vocab*` deals with the vocab, backoff* deals with S2 and `primary*` deals with S1; `data` contains some corpus data that takes a while to load, `util` and `const` are self explanatory.

_This file is [markdown](http://daringfireball.net/projects/markdown/) 
formatted, so if you want, you can generate some nice HTML (or other formats 
with [pandoc](http://johnmacfarlane.net/pandoc/index.html))._

Greg Baker wants you as his TA sometime, given that Anoop wrested you from him this semester.
