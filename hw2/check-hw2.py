import check
import os
import difflib
import re
from view_distance_diff import view_distance_diff

def edgews_normalize(*parts):
    def filter(x):
        x = [l.strip() for l in x]
        return [l + '\n' for l in x if l != '']
    return [filter(x) for x in parts]

def diff_almost_exact(a, b, output):
    a, b = edgews_normalize(a, b)
    if a != b:
            output.write("Diff in output:\n")
            output.writelines(difflib.unified_diff(a, b))
            return False
    return True

def diff_almost_exact_uniq(a, b, output):
    return diff_almost_exact(set(a), set(b), output)

def diff_unordered(a, b, output):
    a, b = edgews_normalize(a, b)
    a, b = sorted(a), sorted(b)
    if a != b:
            output.write("Diff in sorted output:\n")
            output.writelines(difflib.unified_diff(a, b))
            return False
    return True

checks = {
    "cmudict_entries": (diff_unordered, None, []),
    "fst_recognize": (diff_almost_exact, None, []),
    "fst_compose": (diff_almost_exact_uniq, None, []),
    "view_distance": (view_distance_diff, None, []),
#    "viewall_distance": (view_distance_diff, None, []),
#    "transliterate": (diff_almost_exact, None, []),
    "korean_numbers": (diff_almost_exact, None, []),
}

check.check_all(checks)
