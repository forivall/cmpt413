
The filenames for each question are given below.  We will grade
only the questions marked with a dagger in the PDF file.  They are
marked with + below. For the two questions marked with a double
dagger choose one and submit the answer for that one. They are
marked with ++ below.

Test your programs by running: "python check-hw2.py". Run without
arguments and it will print out a detailed help text about usage
and grading.

When you submit HW2 on courses.cs.sfu.ca submit the following URL:

https://punch.cs.sfu.ca/svn/CMPT413-1121-(your-userid)/hw2/answer

%%

+q2: cmudict_entries.py
+q3: fst_recognize.py
q4: fst_compose.py
+q5: view_distance.py
q6: viewall_distance.py
++q7: korean_numbers.py
++q8: transliterate.py 

