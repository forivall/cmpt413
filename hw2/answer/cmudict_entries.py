#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#  

#~ def main(argv):
	#~ 
	#~ return 0
#~ 
#~ if __name__ == '__main__':
	#~ main(sys.argv)

import sys
import re
from itertools import ifilter
from nltk.corpus import cmudict

def filter_stress_markers(entries):
    out = []
    for entry in entries:
        filtered = [re.sub("\d", "", phon) for phon in entry]
        if filtered not in out:
            out.append(filtered)
    return out

stdin = sys.stdin.read()
n = int(stdin)
d = cmudict.dict()
#d__filtered = dict(k, filter_stress_markers(v) for k, v in d)
#d__limited = dict(ifilter(lambda x: len(x[1]), d__filtered.iteritems()))

for k, v in d.iteritems():
    filtered = filter_stress_markers(v)
    if len(filtered) < n:
        continue
    print k, ("\n" + k + " ").join("-".join(phon) for phon in filtered)
