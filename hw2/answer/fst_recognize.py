#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#  

import sys
try:
    from fst import fst
except ImportError:
    import os
    sys.path.insert(1, os.path.join(sys.path[0], '..'))
    from fst import fst

class myFST(fst.FST):
    def recognize(self, input, output):
        #~ out = self.transduce(input.split())
        #~ if (output == " ".join(out)): return True
        #~ else: return False
        inlist = input.split(' ')
        outlist = output.split(' ')
        print >> sys.stderr, 'input( {0} ): {1} ; output( {2} ): {3}'.format( 
                len(inlist), inlist, len(outlist), outlist)
        
        #~ inlist.append(' ') # acts as an epsilon-like character at the end
        #~ outlist.append(' ') # a real solution would also test length below
                
        def gen_states(state, i, o):
            g = lambda:(self.arc_info(arc) for arc in self.outgoing(state))
            return ([(p[1], i, o)     for p in g() 
                            if len(p[2]) == 0 
                            and len(p[3]) == 0] +
                    [(p[1], i, o+1)   for p in g() 
                            if len(p[2]) == 0 
                            and o < len(outlist)
                            and len(p[3]) != 0 and p[3][0] == outlist[o]] +
                    [(p[1], i+1, o)   for p in g() 
                            if i < len(inlist)
                            and len(p[2]) != 0 and p[2][0] == inlist[i]
                            and len(p[3]) == 0] +
                    [(p[1], i+1, o+1) for p in g()
                            if i < len(inlist)
                            and len(p[2]) != 0 and p[2][0] == inlist[i]
                            and o < len(outlist)
                            and len(p[3]) != 0 and p[3][0] == outlist[o]])
        
        agenda = [(self.initial_state, 0, 0)]
        
        #while True:
        for _ in xrange(10000):# make sure it isn't infinite, in case of bugs
            print >> sys.stderr, 'agenda: {0}'.format(list(reversed(agenda)))
            current = agenda.pop()
            
            if (self.is_final(current[0]) and
                    current[1] == len(inlist) and
                    current[2] == len(outlist)):
                return True
            else:
                agenda[:0] = gen_states(*current)
            if len(agenda) <= 0:
                return False
        
        return None
        

def makeFST():
    # example that uses the nltk FST class
    f = myFST('example')

    # first add the states in the FST
    for i in range(1,6):
        f.add_state(label=str(i)) # add states '1' .. '5'

    # add one initial state
    f.initial_state = '1'              # -> 1

    # add all transitions
    f.add_arc('1', '2', ('a'), ('1'))  # 1 -> 2 [a:1]
    f.add_arc('2', '2', ('a'), ('0'))  # 2 -> 2 [a:0]
    f.add_arc('2', '2', ('b'), ('1'))  # 2 -> 2 [b:1]
    f.add_arc('2', '3', (), ('1'))     # 2 -> 3 [:1]
    f.add_arc('3', '4', ('b'), ('1'))  # 3 -> 4 [b:1]
    f.add_arc('4', '5', ('b'), ())     # 4 -> 5 [b:]

    # add final state(s)
    f.set_final('5')                   # 5 ->
    return f

if __name__ == '__main__':
    f = makeFST()
    input_str = raw_input()
    output_str = raw_input()
    #~ input_str, output_str = ("a b a b b", "1 1 0 1 1")
    #~ input_str, output_str = ("a a a a a a a b b", "1 0 0 0 0 0 0 1 1")
    #~ input_str, output_str = ("a a a a b b a b b", "1 0 0 0 1 1 0 1 1")
    #~ input_str, output_str = ("a a b a b b a b b a", "1 0 1 0 1 1 0 1 1 0")
    print f.recognize(input_str, output_str)


