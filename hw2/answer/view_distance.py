#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

import sys

def distance_table(target, source, insertcost, deletecost, replacecost):
    n = len(target)+1
    m = len(source)+1
    # set up dist and initialize values
    dist = [ [0 for j in range(m)] for i in range(n) ]
    for i in range(1,n):
        dist[i][0] = dist[i-1][0] + insertcost
    for j in range(1,m):
        dist[0][j] = dist[0][j-1] + deletecost
    # align source and target strings
    for j in range(1,m):
        for i in range(1,n):
            inscost = insertcost + dist[i-1][j]
            delcost = deletecost + dist[i][j-1]
            if (source[j-1] == target[i-1]): add = 0
            else: add = replacecost
            substcost = add + dist[i-1][j-1]
            dist[i][j] = min(inscost, delcost, substcost)
    # return min edit distance
    return dist

def get_matches(table, target, source, insertcost, deletecost, replacecost):
    n = len(target)
    m = len(source)
    status = []
    while n > 0 and m > 0:
        mindist = min(table[n-1][m-1], table[n][m-1], table[n-1][m])
        #~ dists = (table[n-1][m-1], table[n][m-1], table[n-1][m])
        #~ print '(', table[n-1][m-1], table[n][m-1]
        #~ print ' ', table[n-1][m], table[n][m], ')'
        #~ print '(', table[n-1][m-1], table[n-1][m]
        #~ print ' ', table[n][m-1], table[n][m], ')'
        #~ print mindist,
        diff = table[n][m] - mindist
        diagdiff = table[n][m] - table[n-1][m-1]
        #~ diffs = tuple(table[n][m] - dist for dist in dists)
        #~ if diagdiff == 0:
            #~ print target[n-1], source[m-1]
        if diagdiff == 0 and target[n-1] == source[m-1]:
            n = n-1
            m = m-1
            status.append('|')
        elif diff == deletecost and table[n-1][m] == mindist:# and m > n:
            n = n-1
            source = source[:m] + '_' + source[m:]
            status.append(' ')
        elif diff == insertcost and table[n][m-1] == mindist:# and m > n:
            m = m-1
            target = target[:n] + '_' + target[n:]
            status.append(' ')
        else:
            n = n-1
            m = m-1
            status.append(' ')
    target = '_' * m + target
    source = '_' * n + source
    status.append(' ' * max(m,n))
        
    return target, source, ''.join(reversed(status))

def main(argv=[]):
    if len(argv) < 2:
        return -1
    table = distance_table(argv[0], argv[1], 1, 1, 2)
    print "levenshtein distance =", table[-1][-1]
    #~ print '[' + '\n '.join(str(l) for l in table) + ']'
    
    target, source, status = get_matches(table, argv[0], argv[1], 1, 1, 2)
    print ' '.join(target)
    print ' '.join(status)
    print ' '.join(source)
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

