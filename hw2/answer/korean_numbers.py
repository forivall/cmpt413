#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#  
#  If something isn't working, I'll draw and show you my fst so I can
#  get part marks. My code generates them programmatically because I'm
#  a lazy typist, so it might be hard to follow.
#

import sys
try:
    from fst import fst
except ImportError:
    import os
    sys.path.insert(1, os.path.join(sys.path[0], '..'))
    from fst import fst

def makeFST_intermediate():
    f = fst.FST('number to intermediate representation')
    
    for i in range(0,10):
        f.add_state(label=str(i), is_final=(i != 0 and i != 5))
        
    f.add_state(label='hash')
    f.initial_state = 'hash'
    f.add_arc('hash', '0', (), '#')
    
    f.add_state(label='5\'', is_final=True)
    
    syms = [(), '[10]', '[10^2]','[10^3]','[10^4]','[10]','[10^2]','[10^3]','[10^8]']
    for j in range(0,9):
        for i in range(0,10):
            tout = (str(i),) if syms[j] == () else (syms[j],) if (i==1 and j != 4) else (syms[j],str(i))
            f.add_arc(str(j),str(j+1),(str(i),), tout)
    for i in range(0,10):
        f.add_arc('4', '5\'', (str(i),), (syms[4],))
    return f

digit_mappings = {'1':'il', '2':'i', '3':'sam', '4':'sa', '5':'o', 
                  '6':'yuk', '7':'chil', '8':'pal', '9':'gu'}
placeholder_mappings = {'[10]':'sib','[10^2]':'baek','[10^3]':'cheon',
                         '[10^4]':'man', '[10^8]':'eok'}

import itertools
all_mappings_iter = lambda: itertools.chain(digit_mappings.iteritems(), 
                            placeholder_mappings.iteritems())
                
def makeFST_final():
    f = fst.FST('intermediate representation to final Korean pronounciation')
    
    f.add_state(label='hash')
    f.add_state(label='all', is_final=True)
    f.add_state(label='first_digit', is_final=True)
    f.add_state(label='zero_check')
    
    # I really only have the hash so it's the same as the intermediate repr
    f.initial_state = 'hash'
    
    f.add_arc('hash', 'first_digit', ('#',), ())
    
    for k, v in digit_mappings.iteritems():
        f.add_arc('first_digit', 'all', (k,), (v,))
    f.add_arc('first_digit', 'all', ('0',), ())
    
    for k, v in all_mappings_iter():
        f.add_arc('all', 'all', (k,), (v,))
        f.add_arc('zero_check', 'all', (k,), (v,))
    for k, v in placeholder_mappings.iteritems():
        if k != '[10^4]':
            f.add_arc('all', 'all', (k, '0'), ())
            f.add_arc('zero_check', 'zero_check', (k, '0'), ())
        else:
            f.add_arc('all', 'zero_check', (k, '0'), (v,))
    
    f.add_arc('all', 'all', ('[10^4]', '0', '[10]', '0', '[10^2]', '0', '[10^3]', '0'), ())
    
    
    return f

def main(argv):
    number = raw_input()
    
    # my compose code only will work with single or null values on both
    #   sides of the transducing, so we just transduce it twice instead
    
    fst_i = makeFST_intermediate()
    input_list = list(reversed(number))
    inter = fst_i.transduce(input_list)
    #~ print 'hint:', ' '.join(reversed(inter))
    
    fst_f = makeFST_final()
    final = fst_f.transduce(inter)
    # instead of using the above fst, I could roll out an ad-hoc solution
    #  by extending the below line of code; but the fst works.
    #~ final = [intermediate_mappings[atom] for atom in inter if atom != '#' and atom != '0']
    print ' '.join(reversed(final)) if final is not None else 'NaN'
    
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

