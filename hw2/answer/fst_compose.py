#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#  

import sys
try:
    from fst import fst
except ImportError:
    import os
    sys.path.insert(1, os.path.join(sys.path[0], '..'))
    from fst import fst
from collections import defaultdict

class myFST(fst.FST):
    def compose(self, f):
        new = myFST('composed %s and %s' % (self.label, f.label))
        arcs_info = lambda f:(f.arc_info(arc) for arc in f.arcs())
        
        # cross product
        for state in self.states():
            for fstate in f.states():
                new.add_state('%s_%s' % (state, fstate))
        
        new.initial_state = '%s_%s' % (self.initial_state, f.initial_state)
        
        for state in self.states():
            if self.is_final(state):
                for fstate in f.states():
                    if f.is_final(fstate):
                        new.set_final('%s_%s' % (state, fstate))
        
        # match
        for arc in arcs_info(self):
            for farc in arcs_info(f):
                src, dst, ins, ots = arc
                fsrc, fdst, fins, fots = farc
                if ots == fins:
                    new.add_arc('%s_%s' % (src, fsrc),
                                '%s_%s' % (dst, fdst),
                                ins, fots)
        # prune
        reachable = set([new.initial_state])
        new_states = set([new.initial_state])
        while len(new_states) > 0:
            tmp = set()
            for q in new_states:
                for c in new.states():
                    if any(new.dst(arc) == c for arc in new.outgoing(q)):
                        tmp.add(c)
            new_states = tmp.difference(reachable)
            reachable.update(new_states)
        unreachable = set(new.states()).difference(reachable)
        for state in unreachable:
            new.del_state(state)
        for state in list(new.states()):
            if (len(list(new.outgoing(state))) == 0 and 
                    not new.is_final(state)):
                new.del_state(state)
        
        return new
        
def makeFST1():
    # create FST1
    f1 = myFST('example1')
    # first add the states in the FST
    for i in range(0,4):
        f1.add_state(str(i)) # add states '0' .. '3'

    # add one initial state
    f1.initial_state = '0'              # -> 0

    # add all transitions
    f1.add_arc('0', '0', ('a'), ('a'))  # 0 -> 0 [a:a]
    f1.add_arc('0', '1', ('a'), ('b'))  # 0 -> 1 [a:b]
    f1.add_arc('0', '2', ('b'), ('b'))  # 0 -> 2 [b:b]
    f1.add_arc('1', '3', ('b'), ('a'))  # 1 -> 3 [b:a]
    f1.add_arc('2', '3', ('b'), ('b'))  # 2 -> 3 [b:b]

    # add final state(s)
    f1.set_final('3')                   # 3 ->
    return f1
def makeFST2():
    # create FST2
    f2= myFST('example2')
    # first add the states in the FST
    for i in range(0,3):
        f2.add_state(str(i)) # add states '0' .. '2'

    # add one initial state
    f2.initial_state = '0'              # -> 0

    # add all transitions
    f2.add_arc('0', '1', ('b'), ('a'))  # 0 -> 1 [b:a]
    f2.add_arc('1', '1', ('a'), ('d'))  # 1 -> 1 [a:d]
    f2.add_arc('1', '2', ('b'), ('a'))  # 1 -> 2 [b:a]
    f2.add_arc('1', '2', ('a'), ('c'))  # 1 -> 2 [a:c]

    # add final state(s)
    f2.set_final('2')                   # 2 ->
    return f2
    
if __name__ == '__main__':
    f1 = makeFST1()
    f2 = makeFST2()
    # f1.print_arc_pairs(f2)

    # compose FST1 and FST2
    f3 = f1.compose(f2)
    final = set()
    print f3.label
    print "Initial State:", f3.initial_state
    for arc in f3.arcs():
        print f3.src(arc), f3.dst(arc), f3.in_string(arc), f3.out_string(arc)
        if f3.is_final(f3.dst(arc)):
            final.add(f3.dst(arc))
    print "Final State:", iter(final).next()
