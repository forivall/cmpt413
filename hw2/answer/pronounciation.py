#!/usr/bin/env python2

#from nltk.corpus.reader import cmudict
# print out the documentation string for module cmudict
#print cmudict.__doc__

from nltk.corpus import cmudict
#for word, pron in cmudict.entries():
#    if (pron[-4:] == ['P', 'IH0', 'K', 'S']):
#        print word.lower(),
#print
transcr = cmudict.dict() # warning: this can be very slow
#print transcr['python']
print transcr['store']

print [['S', 'T', 'OW', 'AA']]
