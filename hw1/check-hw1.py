import check
import os

def diff_exact(a, b):
    return a == b

def diff_unordered(a, b):
    return sorted(a) == sorted(b)

def diff_freqs(a, b):
    a, b = [[l.split() for l in x] for x in [a, b]]
    ranks_a, ranks_b = [[l[0] for l in x] for x in [a, b]]
    freqs_a, freqs_b = [sorted(l[1:] for l in x) for x in [a, b]]
    return ranks_a == ranks_b and freqs_a == freqs_b

checks = {
    "noduplicates": (diff_exact, None, []),
    "soundex": (diff_exact, None, []),
    "freq": (diff_freqs, None, []),
    "zipf": (diff_exact, None, [("zipfplot.png", os.path.exists)]),
    "stem_zipf": (diff_exact, None, [("stemplot.png", os.path.exists)]),
    "paircount": (diff_unordered, None, []),
    "virahanka": (diff_exact, None, []),
#    "devowel": (diff_exact, None, []),
#    "silly": (diff_exact, None, []),
#    "reversefile": (diff_exact, None, []),
#    "vsequences": (diff_exact, None, []),
}

check.check_all(checks)
