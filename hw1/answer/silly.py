#!/usr/bin/env python2

silly = "newly formed bland ideas are unexpressible in an infuriating way"

pt_a = silly.split(' ')
print 'a:', pt_a

pt_b = ''.join(s[1] for s in pt_a)
print 'b: ' + pt_b

phrase4 = pt_a[:pt_a.index('an')]
print 'c:', phrase4

print 'd: ', ' '.join(phrase4)

print 'e: '
print '\n'.join(sorted(pt_a))


