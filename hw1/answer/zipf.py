#! /usr/bin/env python2.7

from __future__ import division
from nltk.probability import FreqDist
from nltk import corpus
import pylab
import functools
import numpy

fd = FreqDist(corpus.gutenberg.words('austen-sense.txt'))
freqs = [fd[w] for w in fd.keys()]
pylab.grid(True)
rng = range(0,len(freqs))
pylab.plot(rng, freqs, label='Empirical: Sense & Sensibility')
# for large data use pylab.loglog for a logscale plot

maxfreq = 10**4
#maxfreq = freqs[0]
#print maxfreq
    
sz = fd.B()+1 # number of word types in text

class funcobj(object):
    def __init__(self, mylen, f, *args, **kwargs):
        self.f = functools.partial(f, *args, **kwargs)
        self.mylen = mylen
    def __getitem__(self, idx):
        return self.f(idx + 1)
    def __len__(self):
        return self.mylen

def zipf(k, rank):
    return k / rank
#pylab.plot(funcobj(len(rng), zipf, maxfreq), rng,
#        label="Zipf's Law, k=10^4")
pylab.plot(rng, funcobj(len(rng), zipf, maxfreq),
        label="Zipf's Law, k=10^4")

def mandelbrot(P, B, gamma, rank):
    #return log10(P) - B * log10(rank + gamma)
    return (P * gamma) * ((rank + gamma) ** (-B))
#pylab.plot(funcobj(len(rng), mandelbrot, maxfreq, 1.0, 0.0), rng,
#        label="Mandelbrot B=1 gamma=0")
pylab.plot(rng, funcobj(len(rng), mandelbrot, maxfreq, 1.0, 0.0),
        label="Mandelbrot B=1 gamma=0")
        
#pylab.plot(funcobj(len(rng), mandelbrot, maxfreq, 1.3, 5.0), rng,
#        label="Mandelbrot answer")
pylab.plot(rng, funcobj(len(rng), mandelbrot, maxfreq, 1.30, 10.0),
        label="Mandelbrot answer")
#pylab.plot(rng, funcobj(len(rng), mandelbrot, maxfreq, 1.20, 20.0),
#        label="Mandelbrot answer")

# show all plots
pylab.xlabel("Word Types")
pylab.yscale('log', basey=10)
pylab.xscale('log', basex=10)
pylab.ylabel("Counts")
pylab.legend(loc='lower left')
pylab.savefig("zipfplot.png")
pylab.show()

