#! /usr/bin/env python2.7

import argparse

preamble = '''
reports the linenumbers where each pair of adjacent words occurred in a text
user can set a minimum frequency for each word in the pair
    '''

parser = argparse.ArgumentParser(description=
        "Reports the linenumbers where each pair of adjacent words occurred in "
        "a text. The user can set a minimum frequency for each word in the pair")
parser.add_argument('-n', dest='min_frequency', 
        default=0, type=int, required=False, metavar="freq",
        help='The minimum amount of times a word '
            'has to appear so that it is reported')

args = parser.parse_args()

####

print preamble

import sys
from nltk.probability import FreqDist

stdin = sys.stdin.read()

fd_word = FreqDist(word.lower() for word in stdin.split())

pairs = {}

line_num = 0
for line in stdin.split('\n'):
    line_num = line_num + 1
    
    line_words = []
    
    word_prev = None
    for word in (word.lower() for word in line.split()):
        if fd_word[word] < args.min_frequency:
            word_prev = None
            continue
        if word_prev is None:
            word_prev = word
            continue
        
        word_pair = ' '.join((word_prev, word))
        pairs.setdefault(word_pair, []).append(str(line_num))
        word_prev = word
        
for pair, line_nums in pairs.iteritems():
    print pair, ' '.join(line_nums)


print ''



















