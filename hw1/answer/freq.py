#! /usr/bin/env python2.7

import nltk

def austen_count():
    return wordcount(nltk.corpus.gutenberg.words('austen-sense.txt'))

def wordcount(corpus):
    words = {}
    for word in corpus:
        if word not in words:
            words[word] = 0
        words[word] += 1

    return (item for item in sorted(words.iteritems(), key=lambda x:x[1], reverse=True))

def austen_freq():
    return nltk.probability.FreqDist(nltk.corpus.gutenberg.words('austen-sense.txt'))

if __name__ == '__main__':
    count = 0
    for word, freq in austen_freq().iteritems():
##    for freq, word in austen_count():
        count += 1
        #print count, word, freq
        print count, freq, '\t'+word
    
        
