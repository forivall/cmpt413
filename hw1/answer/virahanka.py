#! /usr/bin/env python2.7

def virahanka(n):
    lookup = [[''], ['S']]
    for m in range(2,n+1):
        lookup.append(['S' + s for s in lookup[m-1]] +
                      ['L' + s for s in lookup[m-2]])
    return lookup[n]

if __name__ == '__main__':
    num = int(raw_input())
    print 'virahanka({0}) = {1}'.format(num, virahanka(num))
    

