word = raw_input()
#word = 'Euler'
#word = 'lloyd'

lookup = {
        'aeiouy':-1,
        'hw':-2,
        'bfpv':1,
        'cgjkqsxz':2,
        'dt':3,
        'l':4,
        'mn':5,
        'r':6,
        }
expanded_lookup = {}
for key, val in lookup.iteritems():
    expanded_lookup.update(dict((ch, val) for ch in key))

soundex = word[0].upper()
last = expanded_lookup[soundex.lower()]
digits = 0
for c in word[1:]:   
    #digit = 0
    digit = expanded_lookup.get(c.lower(),-3)
    if digit < 0 or digit == last:
        if digit == -1:
            last = -1
        continue
    last = digit
    soundex += str(digit)
    digits += 1
    if digits >= 3:
        break

while digits < 3:
    soundex += str(0)
    digits += 1
    
print soundex
