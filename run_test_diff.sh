echo "testcase:"
flags=
if [ -f testcases/$1/$2.hint ]; then
  cat testcases/$1/$2.hint
  flags=-v
fi
if [ -f testcases/$1/$2.out ]; then
  cat testcases/$1/$2.out
fi
echo "run:"
if [ -f testcases/$1/$2.cmd ]; then
  if [ -f testcases/$1/$2.in ]; then
    python2 answer/$1.py $flags `cat testcases/$1/$2.cmd` < testcases/$1/$2.in
  else
    python2 answer/$1.py $flags `cat testcases/$1/$2.cmd`
  fi
else
  if [ -f testcases/$1/$2.in ]; then
    python2 answer/$1.py $flags < testcases/$1/$2.in
  else
    python2 answer/$1.py $flags
  fi
fi

