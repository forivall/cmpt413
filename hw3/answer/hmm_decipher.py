#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q9b: hmm_decipher.py # output: hmmplot.png
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#
#  copyright line is so people don't cheat and I might release under GPL
#
#  to use the hmm files retrieved with gethw.sh, run
#  `runq9.sh python2 answer/hmm_decipher.py`
#

# from os import system
# system('esthmm')
import os
import sys
import subprocess
import tempfile

# print os.environ['PATH']
# set the random seed to 1 to ensure that the same hmm is produced every time
seed = 1
n = 2
m = 27
# hmmfilepath = 'tmp.hmm'
# if os.path.exists(hmmfilepath):
if False:
    hmmfile = open(hmmfilepath)
else:
    hmmfile = tempfile.TemporaryFile()
    # hmmfile = open(hmmfilepath, 'w+')
    proc = subprocess.Popen(('esthmm -S %d -N %d -M %d brownchars_sf.txt'%(seed,n,m)).split(), 
        # stdin=sys.stdin, 
        stdout=hmmfile, 
        # stderr=sys.stderr,
        # cwd=sys.path[0],
        )
    proc.wait()
    hmmfile.seek(0)

a = [None for _ in range(n)]
b = [None for _ in range(n)]
pi = None
for line in hmmfile:
    # if line.strip() == 'A:':
        # for i in range(n):
            # a[i] = [float(x) for x in hmmfile.next().split()]
    if line.strip() == 'B:':
        for i in range(n):
            b[i] = hmmfile.next().split()
    # if line.strip() == 'pi:':
        # pi = [float(x) for x in hmmfile.next().split()]
        
b = [[float(x) for x in l] for l in b]
# print 'a',a
# print 'b',b
# print 'pi',pi

hmmfile.close()

###

# I now know that you used gnuplot, but I made this first, and I don't know how
#  to use it.

import pylab
import string
from itertools import izip, cycle
# pylab.grid(True, color="silver") 
samples = [c for c in string.lowercase] + ['SPC']

vowels = [c for c in 'aeiou']
vowels_b = [[b_[samples.index(v)] for v in vowels] for b_ in b]
for v in vowels:
    i = samples.index(v)
    del samples[i]
    for b_ in b:
        del b_[i]
b = [vb + b_ for vb, b_ in izip(vowels_b, b)]
samples = vowels + samples

pylab.xticks(range(len(samples)), [str(s) for s in samples], rotation=90) 
# pylab.grid(True)
for i, (marker, color) in izip(range(n),cycle([('x','g'),('+','r')])):
    pylab.scatter(range(m), b[i], c=color, marker=marker,
            label="'state%s.emit.plot' using 2.xticlabels(1)" % i)
pylab.ylim(ymin=0.0)
pylab.legend(loc='upper right')
pylab.savefig("hmmplot.png")
# pylab.show()
