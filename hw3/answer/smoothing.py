#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q8: smoothing.py (expected usage: smoothing.usage.txt)
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

'''usage: answer/smoothing.py -h -i trainsection -o testsection -m method

    -h help
    -i training section ,e.g. 'news' or 'editorial'
    -o test section ,e.g. 'news' or 'editorial'
    -m method, e.g. 'no_smoothing', 'interpolation', 'add_one'
    -l lambda_vector, e.g. "0.5:0.3:0.2" for values of \lambda_1, \lambda_2 and \lambda_3.
        It must have 3 elements and sum to 1.0 (only used for interpolation)

    Do not type in the single quotes at the command line.'''

from __future__ import division

### Argument Parsing

method_list = ['no_smoothing', 'interpolation', 'add_one']
lambda_vals = lambda x:[float(i) for i in x.split(':')]

import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser(epilog=
            "Do not type in the single quotes at the command line.",
            usage=__doc__[7:])
    parser.format_help = parser.format_usage
    parser.add_argument('-v', action='store_true')
    parser.add_argument('-i', required=True, metavar="trainsection",
            help=",e.g. 'news' or 'editorial'")
    parser.add_argument('-o', required=True, metavar="testsection",
            help=",e.g. 'news' or 'editorial'")
    parser.add_argument('-m', required=True, metavar="method", choices=method_list,
            help=", e.g. 'no_smoothing', 'interpolation', 'add_one'")
    parser.add_argument('-l', required=False, metavar='lambda_vector',
            help=', e.g. "0.5:0.3:0.2" for values of \lambda_1, \lambda_2 and '
                 '\lambda_3. It must have 3 elements and sum to 1.0 (only used '
                 'for interpolation)', type=lambda_vals)
    args = parser.parse_args()
    if args.m == 'interpolation':
        if args.l is None:
            parser.error('argument -l is required if -m is interpolation')
        elif len(args.l) != 3:
            parser.error('argument -l must have 3 elements')
        elif abs(1.0 - sum(args.l)) > 0.001:
            parser.error('lambda values should sum to 1.0')
else:
    args = argparse.Namespace(i='news', o='editorial', m='no_smoothing', v=True)

### Program

from numpy import log2 as log2_, multiply as multiply_
_NINF = float('-1e300')
ninf_pretty = lambda x:'%.6f' % x if x > _NINF else 'Inf'

import nltk
from nltk.corpus import brown
from nltk.util import ibigrams
from itertools import chain, islice

class JelinekMercerInterpProbDist(nltk.ProbDistI):
    def __init__(self, bigram_pd, unigram_pd, N, lambda_1, lambda_2, lambda_3):
        (self.bigram_pd, self.unigram_pd, 
            self.lambda_1, self.lambda_2, self.lambda_3) = (
            bigram_pd, unigram_pd, lambda_1, lambda_2, lambda_3)
        self.N = N
    def prob(self, sample):
        # N = self.bigram_pd.N()
        # N = self.unigram_pd.N()
        return (self.lambda_1 * self.bigram_pd.freq(sample) +
                self.lambda_2 * self.unigram_pd.freq(sample) + 
                self.lambda_3 / self.N)

second = lambda x:(y[1] for y in x)
pos_tags = lambda c:(second(s) for s in brown.tagged_sents(categories=c))
trainsents = lambda:pos_tags(args.i)
testsents = lambda:islice(pos_tags(args.o), 300)

to_bigrams = lambda s:ibigrams(chain([None],s,[None]))

def logsum(values):
    sum = 0.0
    for i in values:
        if i == _NINF: return _NINF
        sum += i
    if sum < _NINF: return _NINF
    return sum

def log2(val):
    if val == 0.0:
        return _NINF
    return log2_(val)

log2_and_sum = lambda l:logsum(log2(x) for x in l)

# W_T
tag_count = lambda sents:sum((iter_count(s)+1) for s in sents)

# H(T)
def calc_cross_entropy(logprob, N):
    return -(logprob/N)
    
# PP(T)
def calc_perplexity(cross_entropy):
    try:
        return pow(2,cross_entropy)
    except OverflowError:
        return _NINF

def iter_count(it):
    n = 0
    for _ in it:
        n += 1
    return n

no_smoothing = nltk.MLEProbDist
_bins = None
def get_bins(fd):
    global _bins
    if _bins is not None:
        return _bins
#    fd = nltk.FreqDist()
#    for s in trainsents():
#        for b in to_bigrams(s):
#            fd.inc(b)
#    _bins = fd.B()
    _bins = get_unifd().B()
    return _bins
    # return max(fd.B(),1)
    # return fd.B()+1
    
def add_one(fd):
    return nltk.LaplaceProbDist(fd, get_bins(fd))

_unifd = None
def get_unifd():
    global _unifd
    if _unifd is not None:
        return _unifd
    _unifd = nltk.FreqDist()
    for sent in trainsents():
        for word in sent:
            _unifd.inc(word)
        _unifd.inc(None)
    return _unifd

_trainsent_count = None
def get_trainsent_count():
    global _trainsent_count
    if _trainsent_count is not None:
        return _trainsent_count
    _trainsent_count = tag_count(trainsents())
    return _trainsent_count

def interpolation(fd):
    l1, l2, l3 = args.l
    unifd = get_unifd()
    # N = unifd.N()
    N = get_trainsent_count()
    return JelinekMercerInterpProbDist(fd, unifd, N, l1, l2, l3)

method_types = dict((m, globals()[m]) for m in method_list)

cfd = nltk.ConditionalFreqDist()

# train
#for s in trainsents():
## if True:
#    # s = iter(trainsents()).next()
#    s = list(s) # because it's an iterator
#    # print ' '.join(s)
#    for a, b in to_bigrams(s):
#        history = a
#        current_tag = b
#        # print current_tag, history
#        cfd[history].inc(current_tag)
#    print

# condensed:
for s in trainsents():
    for a,b in to_bigrams(s):
        cfd[a].inc(b)

cpd = nltk.ConditionalProbDist(cfd, method_types[args.m])

iter_probs = lambda s:(cpd[a].prob(b) for a,b in to_bigrams(s))
iter_sentence_logprobs = lambda sents:(log2_and_sum(iter_probs(s)) 
        for s in sents)

def print_hint(num_pos, total_probablility, cross_entropy, perplexity):
    if args.v:
        print 'Wt = %d Pt = %.12g cross entropy = %.12g perplexity = %.12g' % (
            num_pos, total_probablility, cross_entropy, perplexity)

def do_calculations(sentence_func):
    W_T = tag_count(sentence_func())
    total_logprob = logsum(iter_sentence_logprobs(sentence_func()))
    h = calc_cross_entropy(total_logprob, W_T)
    pp = calc_perplexity(h)
    print_hint(W_T, total_logprob, h, pp)
    return pp
    
if __name__ == '__main__':
    trainresult = ninf_pretty(do_calculations(trainsents))
    testresult = ninf_pretty(do_calculations(testsents))
    print args.m + ':train:' + trainresult
    print args.m + ':test:' + testresult
    
    
