#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  ++q12b: translate.py
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', required=True, metavar="source",
            help="input martian sentences")
    parser.add_argument('-t', required=True, metavar="target",
            help="output martian sentences")
    args = parser.parse_args()
else:
    args = argparse.Namespace(s='martian_data/b.txt', t='martian_data/a.txt')

from collections import defaultdict
from itertools import izip, chain
import sys
import copy

def read_file(f, sentences, vocab):
    for line in f:
        sentences.append([w for w in line.split() if w != '.'])
        for word in sentences[-1]:
            vocab.add(word)

def make_translation_dict(
        source_vocab, source_sentences, 
        target_vocab, target_sentences):
    possible_translations = len(source_vocab) * len(target_vocab)
    uniform_probability = 1.0/possible_translations
    t = defaultdict(lambda:uniform_probability)
    maxdiff = 1.0
    maxpair = ()
    i = 0
    while maxdiff > 0.025:
        old_t = copy.copy(t)
        i += 1
        c = defaultdict(float)
        total = defaultdict(float)
        
        for e_sent, f_sent in izip(target_sentences, source_sentences):
            for e in sorted(set(e_sent)):
                n_e = e_sent.count(e)
                tot = 0.0
                f_word_types = sorted(set(f_sent))
                for f in f_word_types:
                    tot += t[e,f] * n_e
                for f in f_word_types:
                    n_f = f_sent.count(f)
                    rhs = t[e,f] * n_e * n_f / tot
                    c[e,f] += rhs
                    total[f] += rhs
            for e,f in c:
                    if total[f] > 0:
                        t[e,f] = c[e,f] / total[f]
        maxdiff = max(abs(t[k] - old_t[k]) for k in t.iterkeys())

    translations = defaultdict(lambda:'?')
    translations['.'] = '.'
    translations[None] = '.'
    for w in source_vocab:
        translations[w] = max(((t[u,w], u) for u in target_vocab), key=lambda x:x[0])[1]
    return translations

if __name__ == '__main__':
    source_vocab = set()
    target_vocab = set()

    source_sentences = []
    target_sentences = []
    with open(args.s) as f:
        read_file(f, source_sentences, source_vocab)
    with open(args.t) as f:
        read_file(f, target_sentences, target_vocab)
    translations = make_translation_dict(
            source_vocab, source_sentences,
            target_vocab, target_sentences)
    # #translation_dict.py solution
    for k, v in sorted(translations.items(), key=lambda x:x[0]):
        print k,v
    #translate.py solution
    # for line in sys.stdin:
        # print ' '.join(translations[w] for w in line.split())
