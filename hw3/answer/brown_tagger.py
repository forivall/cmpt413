#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q7: brown_tagger.py (expected usage: brown_tagger.usage.txt)
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

'''usage: answer/brown_tagger.py -h -i trainsection -o testsection -m method

    -h help
    -i training section ,e.g. 'news' or 'editorial'
    -o test section ,e.g. 'news' or 'editorial'
    -m method, e.g. 'default', 'regexp', 'lookup', 'simple_backoff', 'unigram', 'bigram', 'trigram'

    Do not type in the single quotes at the command line.'''
    
method_list = [
        'default',
        'regexp',
        'lookup',
        'simple_backoff',
        'unigram',
        'bigram',
        'trigram'
        ]
    
import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser(epilog=
            "Do not type in the single quotes at the command line.",
            usage=__doc__[7:])
    parser.format_help = parser.format_usage
    parser.add_argument('-i', required=True, metavar="trainsection",
            help=",e.g. 'news' or 'editorial'")
    parser.add_argument('-o', required=True, metavar="testsection",
            help=",e.g. 'news' or 'editorial'")
    parser.add_argument('-m', required=True, metavar="method", choices=method_list,
            help=",e.g. 'default', 'regexp', 'lookup', 'simple_backoff', 'unigram', 'bigram', 'trigram'")

    args = parser.parse_args()
else:
    args = argparse.Namespace(i='news', o='editorial', m='default')

####

import nltk
from nltk.corpus import brown
import re
import copy

import sys
import os
import functools

try:
    import cPickle as pickle
except:
    import pickle

def pickle_cache(f):
    @functools.wraps(f)
    def wrapper(*fargs, **kwargs):
        data_id = '_'.join([os.path.basename(__file__), args.i, f.__name__] +
                list(fargs) + ['%s-%s' % (k,v) for k,v in kwargs.iteritems()])
        fname = os.path.join(sys.path[0], '%s.pickle' % data_id)
        if os.path.exists(fname) and os.path.isfile(fname):
            with open(fname) as pfile:
                return pickle.load(pfile)
        ret = f(*fargs, **kwargs)
        try:
            with open(fname, 'w') as pfile:
                pickle.dump(ret, pfile)
        except: pass
        return ret
    return wrapper

_patterns = [
        (r'.*ing$', 'VBG'),               # gerunds
        (r'.*ed$', 'VBD'),                # simple past
        (r'.*es$', 'VBZ'),                # 3rd singular present
        (r'.*ould$', 'MD'),               # modals
        (r'.*\'s$', 'NN$'),               # possessive nouns
        (r'.*s$', 'NNS'),                 # plural nouns
        (r'^-?[0-9]+(.[0-9]+)?$', 'CD'),  # cardinal numbers
#        (r'.*', 'NN')                     # nouns (default)
        ]

# all of these functions could be changed to purely functional functions by passing
# the args variable instead of using it as a global

patterns = lambda:_patterns

def common_suffixes(n=100):
    suffix_fdist = nltk.FreqDist()
    for word in brown.words(categories=args.i):
        word = word.lower()
        suffix_fdist.inc(word[-1:])
        suffix_fdist.inc(word[-2:])
        suffix_fdist.inc(word[-3:])
    return suffix_fdist.keys()[:n]
    
def common_prefixes(n=50):
    prefix_fdist = nltk.FreqDist()
    for word in brown.words(categories=args.i):
        word = word.lower()
        prefix_fdist.inc(word[:1])
        prefix_fdist.inc(word[:2])
        prefix_fdist.inc(word[:3])
    return prefix_fdist.keys()[:n]
    
def get_pos(regex):
    fdist = nltk.FreqDist()
    c_regex = re.compile(regex)
    for word, pos in brown.tagged_words(categories=args.i):
        if c_regex.match(word.lower()):
            fdist.inc(pos)
    return fdist.keys()[0]

#@pickle_cache
def genpatterns():
    patterns = copy.copy(_patterns)
#    for suffix in common_suffixes(2):
#        regex = r'.*%s$' % suffix
#        pos = get_pos(regex)
#        patterns.append((regex, pos))
    # just adding a few prefixes gives the best performance for simplicity
    for prefix in common_prefixes(5):
        regex = r'%s.*$' % prefix
        pos = get_pos(regex)
        patterns.append((regex, pos))
    return patterns
    
patterns = genpatterns

#@pickle_cache
def lookup_tagger_model():
    fd = nltk.FreqDist(brown.words(categories=args.i))
    cfd = nltk.ConditionalFreqDist(brown.tagged_words(categories=args.i))
    most_freq_words = fd.keys()[:1000]
    return dict((word, cfd[word].max()) for word in most_freq_words)

#@pickle_cache
def most_common_tag():
    brown_tagged = brown.tagged_words(categories=args.i)
    fd = nltk.FreqDist(tag for word, tag in brown_tagged)
    return fd.keys()[0]


default = lambda:nltk.DefaultTagger(most_common_tag())
regexp = lambda:nltk.RegexpTagger(patterns(), backoff=default())
lookup = lambda:nltk.UnigramTagger(model=lookup_tagger_model())
simple_backoff = lambda:nltk.UnigramTagger(model=lookup_tagger_model(),
                backoff=regexp())
unigram = lambda:nltk.UnigramTagger(
                brown.tagged_sents(categories=args.i),backoff=default())
bigram = lambda:nltk.BigramTagger(
                brown.tagged_sents(categories=args.i),backoff=unigram())
trigram = lambda:nltk.TrigramTagger(
                brown.tagged_sents(categories=args.i),backoff=bigram())
l = locals()
methods = dict((m, l[m]) for m in method_list)

performance = lambda:methods[args.m]().evaluate(
                brown.tagged_sents(categories=args.o))

if __name__ == '__main__':
    print '%s:%.6f' % (args.m,performance())

#nltk.help.brown_tagset('.*')
