#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  +q8: smoothing.py (expected usage: smoothing.usage.txt)
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

'''usage: answer/smoothing.py -h -i trainsection -o testsection -m method

    -h help
    -i training section ,e.g. 'news' or 'editorial'
    -o test section ,e.g. 'news' or 'editorial'
    -m method, e.g. 'no_smoothing', 'interpolation', 'add_one'
    -l lambda_vector, e.g. "0.5:0.3:0.2" for values of \lambda_1, \lambda_2 and \lambda_3.
        It must have 3 elements and sum to 1.0 (only used for interpolation)

    Do not type in the single quotes at the command line.'''

from __future__ import division

### Argument Parsing

method_list = ['no_smoothing', 'interpolation', 'add_one']
lambda_vals = lambda x:[float(i) for i in x.split(':')]

import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser(epilog=
            "Do not type in the single quotes at the command line.",
            usage=__doc__[7:])
    parser.format_help = parser.format_usage
    parser.add_argument('-v', action='store_true')
    parser.add_argument('-i', required=True, metavar="trainsection",
            help=",e.g. 'news' or 'editorial'")
    parser.add_argument('-o', required=True, metavar="testsection",
            help=",e.g. 'news' or 'editorial'")
    parser.add_argument('-m', required=True, metavar="method", choices=method_list,
            help=", e.g. 'no_smoothing', 'interpolation', 'add_one'")
    parser.add_argument('-l', required=False, metavar='lambda_vector',
            help=', e.g. "0.5:0.3:0.2" for values of \lambda_1, \lambda_2 and '
                 '\lambda_3. It must have 3 elements and sum to 1.0 (only used '
                 'for interpolation)', type=lambda_vals)
    args = parser.parse_args()
    if args.m == 'interpolation':
        if args.l is None:
            parser.error('argument -l is required if -m is interpolation')
        elif len(args.l) != 3:
            parser.error('argument -l must have 3 elements')
        elif abs(1.0 - sum(args.l)) > 0.001:
            parser.error('lambda values should sum to 1.0')
else:
    args = argparse.Namespace(i='news', o='editorial', m='no_smoothing', v=True)

### Program

from numpy import log2 as log2_, multiply as multiply_
import math
_NINF = float('-1e300')
ninf_pretty = lambda x:'%.6f' % x if x > _NINF else 'Inf'
from nltk.corpus import brown
from itertools import islice, izip, chain
from nltk.probability import ConditionalFreqDist, ConditionalProbDist, \
    FreqDist, MLEProbDist, ProbDistI, LaplaceProbDist
from nltk.util import ibigrams

#### Helper Functions
def logsum(values):
    sum = 0.0
    for i in values:
        if i == _NINF: return _NINF
        sum += i
    if sum < _NINF: return _NINF
    return sum

def log2(val):
    if val == 0.0:
        return _NINF
    return log2_(val)

chainlogsum = lambda l:logsum(log2(x) for x in l)
chainmul = lambda x:reduce(multiply_, x)

extract_second = lambda iter:(i[1] for i in iter)
#~ postag_bigrams = lambda s:izip(
        #~ chain(extract_second(s),[None]),
        #~ chain([None],extract_second(s)))
postag_bigrams = lambda s:ibigrams(chain([None],extract_second(s),[None]))

def get_freqdist(iter):
    fd = FreqDist()
    #~ for sent in islice(brown.tagged_sents(categories=args.i), 5):
    for sent in iter:
        for bigram in postag_bigrams(sent):
            fd.inc(bigram)
    return fd

# P(s) -- This is equation 1
def calc_sentence_prob(sentences, pd):
    # if args.v:
        # sent = sentences[0]
        # print [pd.prob(bigram) for bigram in postag_bigrams(sent)]
    return [chainmul(pd.prob(bigram) for bigram in postag_bigrams(sent)) for sent in sentences]
        
def calc_sentence_logprob(sentences, pd):
    prob_sums = [logsum(log2(pd.prob(bigram)) for bigram in postag_bigrams(sent))
    # return [logsum(math.log(pd.prob(bigram)) for bigram in postag_bigrams(sent))
        for sent in sentences]
    # for prob_sum, sentence in izip(prob_sums, sentences):
        # print '%5.2f %s' % (prob_sum,' '.join(w[1] for w in sentence))
    return prob_sums

# Wt = FreqDist.N()

# H(T)
def calc_cross_entropy(logprob, fd):
    return -(logprob/fd.N())
    
# PP(T)
def calc_perplexity(cross_entropy):
    try:
        return pow(2,cross_entropy)
    except OverflowError:
        return _NINF

#### Methods

class JelinekMercerInterpProbDist(ProbDistI):
    def __init__(self, bigram_pd, unigram_pd, lambda_1, lambda_2, lambda_3):
        (self.bigram_pd, self.unigram_pd, 
            self.lambda_1, self.lambda_2, self.lambda_3) = (
            bigram_pd, unigram_pd, lambda_1, lambda_2, lambda_3)
    def prob(self, sample):
        N = self.bigram_pd.N() # TODO: calculate the actual N
        return (self.lambda_1 * self.bigram_pd.freq(sample) +
                self.lambda_2 * self.unigram_pd.freq(sample[0]) + 
                self.lambda_3 / N)
    

class SmoothingMethod(object):
    def __init__(self, args):
        self.trainsection = args.i
        self.testsection = args.o
    def train(self):
        return self.do_calculations(self.trainsents, True)
    def test(self):
        return self.do_calculations(self.testsents, False)
    def trainsents(self):
        return brown.tagged_sents(categories=self.trainsection, 
                # simplify_tags=True,
                )
    def testsents(self):
        return islice(brown.tagged_sents(categories=self.testsection,
                # simplify_tags=True,
                ), 300)
    def do_calculations(self, sentence_func, is_training=False):
        fd = get_freqdist(sentence_func())
        if is_training:
            self.pd = self.probdistclass(fd)
        log_text_prob = (logsum(calc_sentence_logprob(sentence_func(), self.pd)) 
                        # / log2(len(list(sentence_func())))
                        )
        h = calc_cross_entropy(log_text_prob, fd)
        pp = calc_perplexity(h)
        self.print_hint(fd.N(), log_text_prob, h, pp)
        return pp
    
    @staticmethod
    def print_hint(num_pos, total_probablility, cross_entropy, perplexity):
        if args.v:
            print 'Wt = %d Pt = %.12g cross entropy = %.12g perplexity = %.12g' % (
                num_pos, total_probablility, cross_entropy, perplexity)

class no_smoothing(SmoothingMethod):
    probdistclass = MLEProbDist
class interpolation(SmoothingMethod):
    def __init__(self, args):
        super(interpolation, self).__init__(args)
        self.lambda_1, self.lambda_2, self.lambda_3 = args.l
    def probdistclass(self, fd):
        unifd = FreqDist()
        for sent in self.trainsents():
            for unigram in extract_second(sent):
                unifd.inc(unigram)
        return JelinekMercerInterpProbDist(fd, unifd, 
                self.lambda_1, self.lambda_2, self.lambda_3)
class add_one(SmoothingMethod):
    probdistclass = LaplaceProbDist
        
methods = dict((m, globals()[m]) for m in method_list)

if __name__ == '__main__':
    method = methods[args.m](args)
    trainresult = ninf_pretty(method.train())
    testresult = ninf_pretty(method.test())
    print args.m + ':train:' + trainresult
    print args.m + ':test:' + testresult
        

