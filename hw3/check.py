"""
usage: %s [-B] [-o DIR] [-d] ANSWER-DIR TESTCASE-DIR [...]

Options:

-B      Disable backups of existing extra files produced by the
        programs. If set then the existing files will be overwritten.

-o DIR  Save the output from the testcases to DIR. Will overwrite
        existing files in DIR. Useful for debugging your code.

-d      Show a comparison of outputs in a diff-like format for each
        failed testcase. For testcases where output is not matched
        exactly, the diff will show a comparison where the output
        and expected output are in a normalized form. Read the diff
        functions (listed below for each program) if the diff is not
        clear.

-s FILE Skip all testcases indicated by FILE. Each line in FILE is
        either a program name (without the path and the .py extension),
        in which case all testcases for that program will be skipped;
        or a program name and a testcase name, in which case the
        specific testcase will be skipped for that program.

ANSWER-DIR      Directory where your answer programs are stored
TESTCASE-DIR    Directory with test cases for programs to be tested

The programs are run with ANSWER-DIR as the current directory. Any
files created without an explicit path by your programs will be
saved to ANSWER-DIR.

For each Python program to be tested: program.py, there is a directory
TESTCASE-DIR/program

Inside this directory there are the following types of files where
NN stands for a number like 0, 1, ...:

NN.file : contains filename used as input to program.py
NN.in   : input sent to standard input (stdin) of program.py
            NN.file and NN.in should not simultaneously exist.
NN.cmd  : the command line argument to program.py, 
            e.g. for "program.py -n 10", 0.cmd would contain "-n 10" 
NN.out  : expected output printed by program.py to standard output (stdout)
NN.err  : expected output printed by program.py to standard error (stderr)
NN.fail : indicates that program.py should fail

Note that NN.out is the "expected" output. There may be differences
caused by different OS or NLTK_DATA issues so run check.py on CSIL
Linux machines for the defnitive test. On CSIL Linux machines the
outputs should match if your program is correct. If you are convinced
your program is right but the outputs do not match on the CSIL
machines, then email the instructor and the TA.

If your program succeeds then it should exit with zero status
(sys.exit(0) or reaching the end of the program). If it fails (eg
due to invalid input) then it should exit with non-zero status (eg
sys.exit(1)). Testcases with a NN.fail file are expected to fail.

We only check stdout and stderr. In some cases, we also specify
output files to be created by your program, e.g. png files for
graphs. These files are checked for existence, but we do not parse
those files for correctness. The TA will grade these files by hand.

Authors: Max Whitney (mwhitney@cs.sfu.ca) and Anoop Sarkar (anoop@cs.sfu.ca).
"""

import sys
import os
import os.path
import tempfile
import subprocess
import collections
import shlex
import pprint
import operator

def run(argv, stdin_file=None, output_path=None):
    """
    Runs a command specified by an argument vector (including the program name)
    and returns lists of lines from stdout and stderr.
    """
    if output_path is not None:
        dir = os.path.dirname(output_path)
        if not os.path.exists(dir):
            os.makedirs(dir)
        stdout_fn = "%s.out" % (output_path)
        stderr_fn = "%s.err" % (output_path)
        stdout_file = open(stdout_fn, 'w')
        stderr_file = open(stderr_fn, 'w')
        status_fn = "%s.ret" % (output_path)
    else:
        stdout_file, stdout_fn = tempfile.mkstemp("stdout")
        stderr_file, stderr_fn = tempfile.mkstemp("stderr")
        status_fn = None
    try:
        prog = subprocess.Popen(argv, stdin=stdin_file or subprocess.PIPE, stdout=stdout_file, stderr=stderr_file, close_fds=True)
        if stdin_file is None:
            prog.stdin.close()
        prog.wait()
        if status_fn is not None:
            with open(status_fn, 'w') as status_file:
              print >> status_file, prog.returncode
        with open(stdout_fn) as stdout_input:
            stdout_lines = list(stdout_input)
        with open(stderr_fn) as stderr_input:
            stderr_lines = list(stderr_input)
        return stdout_lines, stderr_lines, prog.returncode
    except Exception:
        if output_path is None:
            os.remove(stdout_fn)
            os.remove(stderr_fn)
        raise

def check_output(gold_path, output_lines, check, diff_output=None):
    if check is not None:
        if os.path.exists(gold_path):
            with open(gold_path) as file:
                lines = list(file)
        else:
            lines = []
        try:
            use_diff_output = open(os.devnull, 'w') if diff_output is None else diff_output
            return check(lines, output_lines, use_diff_output)
        except Exception:
            if diff_output is None:
                use_diff_output.close()
            raise
    else:
        return True

def backup_files(dir, *filenames):
    for filename in filenames:
        if os.path.exists(filename):
            file, temp_path = tempfile.mkstemp(dir=dir, prefix=filename + ".bak")
            os.close(file)
            os.remove(temp_path) # Apparently on Windows we have to remove the file before copying over it
            print >> sys.stderr, "renaming existing output file \"%s\" to \"%s\"" % (filename, temp_path)
            os.rename(filename, temp_path)
            assert not os.path.exists(filename)

def run_testcase(path, program, testcase, check, do_backups=True, output_path=None, diff_output=None):
    program = "%s.py" % (program)
    check_stdout, check_stderr, file_checks = check

    file_path = os.path.join(path, "%s.file" % (testcase))
    input_path = os.path.join(path, "%s.in" % (testcase))
    args_path = os.path.join(path, "%s.cmd" % (testcase))
    gold_stdout_path = os.path.join(path, "%s.out" % (testcase))
    gold_stderr_path = os.path.join(path, "%s.err" % (testcase))
    fail_path = os.path.join(path, "%s.fail" % (testcase))

    if os.path.exists(file_path):
        if os.path.exists(input_path):
            print >> sys.stderr, "warning: testcase %s for %s has both a .file and a .in file"
        with open(file_path) as file_file:
            input_path = os.path.join(path, file_file.read().strip())

    if os.path.exists(args_path):
        with open(args_path) as file:
            args = shlex.split(file.read())
    else:
        args = []

    if do_backups:
        backup_files(".", *(fn for (fn, fc) in file_checks))

    input_file = None
    try:
        if os.path.exists(input_path):
            input_file = open(input_path)
        print >> sys.stderr, "running", program, testcase
        stdout, stderr, status = run([sys.executable, program] + args, input_file, output_path=output_path)
        failed = []
        if os.path.exists(fail_path):
            if status == 0:
                failed.append("success exit value")
        else:
            if status != 0:
                failed.append("failure exit value")
        if not check_output(gold_stdout_path, stdout, check_stdout, diff_output):
            failed.append("stdout")
        if not check_output(gold_stderr_path, stderr, check_stderr, diff_output):
            failed.append("stderr")
        for file_check_path, file_check in file_checks:
            if not file_check(file_check_path):
                failed.append(file_check_path)
        print >> sys.stdout, program, testcase, ":",
        if len(failed) > 0:
            print >> sys.stdout, "failed (%s)" % (','.join(failed))
            return False
        else:
            print >> sys.stdout, "passed"
            return True
    except Exception:
        if input_file is not None:
            input_file.close()
            print >> sys.stderr
        raise

def run_testcases(path, programs, choose_check, do_backups=True, output_path=None, diff_output=None, skip=set()):
    counts = {}
    no_program = set()
    for program in programs:
        counts.setdefault(program, (0, 0))
        check = choose_check(program)
        subdir_path = os.path.join(path, program)
        testcases = set(f[:(i if i >= 0 else None)] for f in os.listdir(subdir_path) if not f[0] == '.' for i in [f.find('.')]) if os.path.isdir(subdir_path) else []
        program_found = os.path.exists("%s.py" % (program))
        if not program_found:
            no_program.add(program)
            print >> sys.stderr, "warning: skipping %s because %s.py does not exist" % (program, program)
        if len(testcases) > 0:
            for testcase in testcases:
                if (program,) in skip or (program, testcase) in skip:
                    print >> sys.stderr, "warning: skipping %s %s by manual choice" % (program, testcase)
                elif check is None:
                    print >> sys.stderr, "warning: skipping %s %s because there is no check for it" % (program, testcase)
                else:
                    if program_found:
                        output = os.path.join(output_path, program, testcase) if output_path is not None else None
                        passed = run_testcase(subdir_path, program, testcase, check, do_backups=do_backups, output_path=output, diff_output=diff_output)
                    else:
                        passed = False
                    correct, total = counts.get(program)
                    counts[program] = (correct + int(passed), total + 1)
        else:
            print >> sys.stderr, "warning: no testcases for %s" % (program)

    return counts, no_program

def usage(progname, checks):
    pp = pprint.PrettyPrinter(indent=4)
    print >> sys.stderr, __doc__ % (progname)
    print >> sys.stderr, "Current programs that are tested:"
    def fun_name(function):
      return function.__name__ if function is not None else "None"
    for name, (check_stdout, check_stderr, file_checks) in checks.iteritems():
      print >> sys.stderr, "\t%s %s" % (name, " ".join("%s:%s" % (l, fun_name(f)) for (l, f) in [("stdout", check_stdout), ("stderr", check_stderr)] + file_checks))

def check_all(checks, argv=sys.argv):
    import getopt

    try:
        opts, args = getopt.getopt(argv[1:], "Bo:ds:")
        do_backups = True
        output_path = None
        diff_output = None
        skip = set()
        for opt, value in opts:
            if opt == "-B":
                do_backups = False
            elif opt == "-o":
                output_path = os.path.abspath(value)
            elif opt == "-d":
                diff_output = sys.stdout
            elif opt == "-s":
                with open(value) as file:
                    skip = set(tuple(line.split()) for line in file)
        if len(args) < 2:
            raise getopt.GetoptError("Not enough arguments.")
    except getopt.GetoptError, e:
        usage(sys.argv[0], checks)
        sys.exit(1)

    answer_dir = args[0]
    testcase_paths = [os.path.abspath(p) for p in args[1:]]
    os.chdir(answer_dir)
    for testcase_path in testcase_paths:
        counts, no_program = run_testcases(testcase_path, checks.keys(), checks.get, do_backups=do_backups, output_path=output_path, diff_output=diff_output, skip=skip)
        for program, (correct, total) in counts.iteritems():
            print "%s : %i / %i%s" % (program, correct, total, " (no program)" if program in no_program else "")
        print "total : %i / %i" % (sum(c for (c, t) in counts.itervalues()), sum(t for (c, t) in counts.itervalues()))
