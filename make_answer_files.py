#! /usr/bin/env python2
import sys

if len(sys.argv) > 1:
    hwdir = sys.argv[1]
    with open("%s/readme.txt" % (hwdir,)) as readme:
        for line in readme:
            if line.strip() == "%%":
                break
        for line in readme:
            line = line.strip()
            if line:
                split_line = line.split(':',1)
                fname = split_line[1].split()[0]
                print 'making',fname
                f = open('%s/answer/%s' % (hwdir,fname),'a')
                f.write(
'''#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  %s
#  
#  Copyright 2012 Jordan Klassen <jjk5@sfu.ca>
#

''' % (line,))
                f.close()
else:
    print 'usage: %s homework_directory' % sys.argv[0]
